package com.zmast.crud.service;

import java.util.List;

import com.zmast.crud.model.Item;

public interface ItemService {
	void save(Item cricketer);
	Item findById(Long id);
	List<Item> getAllItems();	
}
