
package com.zmast.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.zmast.crud.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item,Long>{

}
