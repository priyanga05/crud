import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {ItemComponent} from './item/item.component';
import {ListItemComponent} from './item/list-item.component';

const routes: Routes = [
 
  { path: 'additem', component: ItemComponent },
  { path: 'edit-item', component: ItemComponent },  
  { path: 'listitem', component: ListItemComponent },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
