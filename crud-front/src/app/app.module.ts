import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing.module';

import { ItemService} from './item/item.service';
import { HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import { ItemComponent } from './item/item.component';
import { ListItemComponent } from './item/list-item.component';


@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ListItemComponent
    

 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,    
    ReactiveFormsModule
   
    ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
